# glox

The tree-walk interpreter from [Crafting
Interpreters](https://www.craftinginterpreters.com), written in Go.

## Running

``` shell
go build
./glox
```
