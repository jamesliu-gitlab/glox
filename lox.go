package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var hadError bool

func main() {
	if len(os.Args) > 2 {
		fmt.Println("Usage: ./glox [script]")
		os.Exit(64)
	}
	if len(os.Args) == 2 {
		log.Fatal(runFile(os.Args[1]))
	} else {
		runPrompt()
	}
}

func runFile(filename string) error {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return fmt.Errorf("read lox source: %w", err)
	}

	run(string(b))
	if hadError {
		os.Exit(65)
	}

	return nil
}

func runPrompt() {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print("> ")
		if scanner.Scan() {
			line := scanner.Text()
			if line == "" {
				break
			}
			run(line)
			hadError = false
		}

		if err := scanner.Err(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}

func run(source string) {
	scanner := NewScanner(source)
	tokens := scanner.ScanTokens()

	for _, token := range tokens {
		fmt.Println(token)
	}
}

func loxError(line int, message string) {
	report(line, "", message)
	hadError = true
}

func report(line int, where, message string) {
	_, _ = os.Stderr.WriteString(fmt.Sprintf("[line %d] Error %s: %s\n", line, where, message))
}
