package main

import "fmt"

type TokenType string

const (
	LeftParen  TokenType = "LEFT_PAREN"
	RightParen           = "RIGHT_PAREN"
	LeftBrace            = "LEFT_BRACE"
	RightBrace           = "RIGHT_BRACE"
	Comma                = "COMMA"
	Dot                  = "DOT"
	Minus                = "MINUS"
	Plus                 = "PLUS"
	Semicolon            = "SEMICOLON"
	Slash                = "SLASH"
	Star                 = "STAR"

	Bang         = "BANG"
	BangEqual    = "BANG_EQUAL"
	Equal        = "EQUAL"
	EqualEqual   = "EQUAL_EQUAL"
	Greater      = "GREATER"
	GreaterEqual = "GREATER_EQUAL"
	Less         = "LESS"
	LessEqual    = "LESS_EQUAL"

	Identifier = "IDENTIFIER"
	String     = "STRING"
	Number     = "NUMBER"

	And    = "AND"
	Class  = "CLASS"
	Else   = "ELSE"
	False  = "FALSE"
	Fun    = "FUN"
	For    = "FOR"
	If     = "IF"
	Nil    = "NIL"
	Or     = "OR"
	Print  = "PRINT"
	Return = "RETURN"
	Super  = "SUPER"
	This   = "THIS"
	True   = "TRUE"
	Var    = "VAR"
	While  = "WHILE"

	EOF = "EOF"
)

type Token struct {
	ty      TokenType
	lexeme  string
	literal any
	line    int
}

func NewToken(ty TokenType, lexeme string, literal any, line int) Token {
	return Token{
		ty:      ty,
		lexeme:  lexeme,
		literal: literal,
		line:    line,
	}
}

func (t Token) String() string {
	return fmt.Sprintf("%s %s %+v", t.ty, t.lexeme, t.literal)
}
